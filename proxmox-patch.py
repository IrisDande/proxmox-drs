import os
import time
import argparse
import config
from datetime import timedelta, datetime

import salt.client

parser = argparse.ArgumentParser()
parser.add_argument("-n", "--node", required=True, help="Node name to be patched")
args = vars(parser.parse_args())

maint_path = config.RESOURCE_SCHEDULER['maint_path']

# Basic house keeping checks of config file variables
if not maint_path.endswith('/'):
    maint_path = maint_path +  "/"

file = maint_path + args['node'] + ".maintenance"
# Wait x minutes for the node to go into maintenance mode
# This will have to be adjusted bepending on how busy the node is
wait_delta = 10 # minutes


def createMaintFile(filename):
    # Create the maintenance marker file used by Proxmox-DRS.
    # Proxmox-DRS will evacuate VMs from the node to be patched
    if os.path.exists(filename):
        print("The file {} already exists.".format(filename))
        return(1)

    open(filename, "a").close()
    print("{} created.".format(filename))
    print("Monitoring the file for confirmation node is in maintenance mode")

    return(0)

def waitNodeInMaint(filename):
    # Once Proxmox-DRS has evacuated VMs it updates the contents of the
    # maintenance file to signify all VMs have been moved
    s_time = datetime.now()
    while True:
        if s_time + timedelta(minutes=wait_delta) < datetime.now():
            return(1)
        if os.path.exists(filename):
            filesize = os.path.getsize(filename)
            time.sleep(5)
            if filesize > 0:
                f = open(filename, "r")
                line = f.readline()
                f.close()
                line = line.replace('\n', '')
                if line.lower() == 'true':
                    print("Node {} is in maintenance mode".format(args['node']))
                    break

    return(0)


def pv_version(node, state):
    # Use the Salt to save pveversion output
    print("Collect pveversion for {}".format(node))
    outputfile = '/usr/bin/pveversion -v > /root/' + state + '-pveversion-$(date +%Y-%m-%d).txt'
    local = salt.client.LocalClient()
    output = local.cmd(node, 'cmd.run', [outputfile])

    # As long as the Salt output does not show 'False'
    # we consider the update was successful
    if output is not None:
        for key, value in output.items():
            if key == node and value == 'False':
                print("Error: {}".format(output))
                return(1)

    return(0)

def pv_patchNode(node):
    # Use the Salt state 'proxmox' (/srv/salt/base/proxmox/init.sls)
    # to patch the node.
    pv_version(node, 'prepatch')
    print("Salt patch {}".format(node))
    local = salt.client.LocalClient()
    output = local.cmd(node, 'state.apply', ['proxmox'])

    # As long as the Salt output does not show 'False'
    # we consider the update was successful
    if output is not None:
        for key, value in output.items():
            if key == node and value == 'False':
                print("Error: {}".format(output))
                return(1)

    pv_version(node, 'postpatch')
    print(output)
    return(0)


def main():
    # Create the maintenance marker file
    if createMaintFile(file):
        print("The maintenance file {} already exists.".format(file))
        print("Checking to see if the VMs have been migrated")

    # Wait for Proxmox-DRS to migrate all VMs of the node
    if not waitNodeInMaint(file):
        # Patch the node using Salt states
        if not pv_patchNode(args['node']):
            print("Patching for {} successful".format(args['node']))
            print("{} has been left in maintenance mode.".format(args['node']))
            print("To take the node out of maintenance mode, run the following command:")
            print("rm {}".format(file))
        else:
            print("Error Salt update failed")
            print("{} has been left in maintenance mode.".format(args['node']))
    else:
        print("Error while waiting for node to go into maintenance")
        exit(1)

if __name__ == "__main__":
    main()
# 
# Proxomx dynamic resource system
#
# Now that I have a great domain name, I can start
# to work on my novel.
#

from proxmoxer import ProxmoxAPI
import operator
import time
import config
import logging
import logging.handlers
import os

my_logger = logging.getLogger('proxmox_drs')
my_logger.setLevel(logging.INFO)
handler = logging.handlers.SysLogHandler(address='/dev/log')
my_logger.addHandler(handler)

proxmox_host = config.PROXMOX_HOST['host']
proxmox_user = config.PROXMOX_HOST['user']
proxmox_pw = config.PROXMOX_HOST['password']
proxmox_ssl = config.PROXMOX_HOST['verify_ssl']

try:
    proxmox = ProxmoxAPI(proxmox_host, user=proxmox_user, password=proxmox_pw, verify_ssl=proxmox_ssl)
except Exception as e:
    print("[Proxmox-DRS][ERROR] Proxmox API connection error: {0}".format(e))
    my_logger.error("[Proxmox-DRS][ERROR] Proxmox API connection error: {0} - exiting".format(e))
    exit(1)

# Value between 1 and 10
# this value loops the checks and migrations to move
# more VMs per run of the script
drs_aggression = config.RESOURCE_SCHEDULER['drs_aggression']
top_vms = config.RESOURCE_SCHEDULER['top_vms']
min_vms = config.RESOURCE_SCHEDULER['min_vms']
maint_path = config.RESOURCE_SCHEDULER['maint_path']

# Basic house keeping checks of config file variables
if not maint_path.endswith('/'):
    maint_path = maint_path +  "/"

if drs_aggression > 10:
    drs_aggression = 10
    
    
def pv_getLoad(inMaint):
    # List all Proxmox nodes and collect CPU data from the API
    # Calculate the moderately loaded band using CSLB
    # output a list of nodes and where they fit in the band

    # Loop through all nodes in the cluster and collect CPU percentage
    pv_stats = {}
    try:
        for node in proxmox.nodes.get():
            #print(node['cpu'], node['node'])
            if node['node'] != inMaint:
                pv_stats[node['node']] = node['cpu'] * 100        
    except Exception as e:
        print("[Proxmox-DRS][ERROR] Get Node load failed: {0}".format(e))
        my_logger.error("[Proxmox-DRS][ERROR] Get Node load failed: {0}".format(e))
        return()

    # Sort nodes into lowest to highest CPU
    pv_stats = sorted(pv_stats.items(), key=operator.itemgetter(1))
    
    # Add all CPU percentages together
    cpu_total = 0
    for (node, cpu) in pv_stats:
        #print("{0} {1}".format(node, cpu))
        cpu_total += cpu
    
    # Get CPU average
    pv_avg = cpu_total / len(pv_stats)
    
    # Get CPU mean
    pv_mean = (pv_stats[0][1] + pv_stats[-1][1]) / 2.0
        
    # Get absolute different between average and mean
    pv_diff = abs(pv_avg - pv_mean)
    
    # Set low and high CPU thresholds to create band
    if pv_diff > 10:
        load_low = pv_avg - pv_diff
        load_high = pv_avg + pv_diff
    else:
        load_low = pv_avg - 10.0
        load_high = pv_avg + 10.0
        
    #print("low: {0}, high: {1}".format(load_low, load_high))
    
    index = 0
    pv_band = []
    # Compare node CPUs to band
    for (node, cpu) in pv_stats:
        if cpu > load_low and cpu < load_high:
            print("[Proxmox-DRS] {0} cpu: {1} - OK".format(node, cpu))
            my_logger.info("[Proxmox-DRS] {0} cpu: {1} - OK".format(node, cpu))
            pv_band.append({'node': node, 'index': index, 'load': 'OK'})
        if cpu < load_low:
            print("[Proxmox-DRS] {0} cpu: {1} - Low Load".format(node, cpu))
            my_logger.info("[Proxmox-DRS] {0} cpu: {1} - Low Load".format(node, cpu))
            pv_band.append({'node': node, 'index': index, 'load': 'LOW'})
        if cpu > load_high:
            print("[Proxmox-DRS] {0} cpu: {1} - High Load!".format(node, cpu))
            my_logger.info("[Proxmox-DRS] {0} cpu: {1} - High Load!".format(node, cpu))
            pv_band.append({'node': node, 'index': index, 'load': 'HIGH'})
        index += 1
        
    return(pv_band)


def pv_getVMs(node):
    # Get a list of all VMs on the node
    pv_vms = []

    try:
        for vm in proxmox.nodes(node).qemu.get():
            if vm['status'] == 'running':
                pv_vms.append({'name': vm['name'], 'cpu': vm['cpu'] * 100, 'memory': vm['mem'], 'vmid': vm['vmid'], 'type': 'qemu'})
            
        # Sort the list from highest CPU to lowest
        pv_vms = sorted(pv_vms, key=operator.itemgetter('cpu'), reverse=True)
    except Exception as e:
        print("[Proxmox-DRS][ERROR] pv_getVMs: {0}".format(e))
        my_logger.error("[Proxmox-DRS][ERROR] pv_getVMs: {0}".format(e))
        pv_vms = None
        exit()

    return(pv_vms)
    
    
def pv_migrate(vm, s_node, d_node):
    print("[Proxmox-DRS] Migrating {0} from {1} to {2}".format(vm, s_node, d_node))
    my_logger.info("[Proxmox-DRS] Migrating {0} from {1} to {2}".format(vm, s_node, d_node))
    try:
        node = proxmox.nodes(s_node)
        ret = node.qemu(vm).migrate.create(target=d_node, online=1)
    except Exception as e:
        print("[Proxmox-DRS][ERROR] pv_migrate: {0}".format(e))
        my_logger.error("[Proxmox-DRS][ERROR] pv_migrate: {0}".format(e))
        ret = ""
    
    return(ret)
    
def pv_getTasks(task_id):
    # check the migration task has completed
    # check for 5 minutes then give up    
    status = 'Processing'
    try:
        for i in range(0, 60):
            tasks = proxmox.cluster.tasks.get()
            for task in tasks:
                if task['upid'] == task_id:
                    if task.get('status') != None:
                        status = task['status']
                        break
            if status != 'Processing':
                break
            time.sleep(5)
    except Exception as e:
        print("[Proxmox-DRS][ERROR] Get PV Tasks failed: {0}".format(e))
        my_logger.error("[Proxmox-DRS][ERROR] Get PV Tasks failed: {0}".format(e))
    
    if status != 'OK':
        status = "Migration did not complete in 5 minutes"
                
    return(status)

def pv_processMigration(pv_load):
    # migrate VMs from the highest loaded node to the lowest loaded.
    
    # source node (highest CPU)
    s_node = pv_load[-1]['node']
    # destination node (lowest CPU)
    d_node = pv_load[0]['node']
    
    # Get a list of all VMs on the source node
    pv_vms = pv_getVMs(s_node)

    if len(pv_vms) > min_vms:
        # Get the top n VMs by CPU
        pv_vms = pv_vms[:top_vms]
        
        #print("last vm: {0}".format(pv_vms[-1]))
        task_id = pv_migrate(pv_vms[-1]['vmid'], s_node, d_node)
        
        status = pv_getTasks(task_id)
        print("[Proxmox-DRS] Migration status: {0}".format(status))
        my_logger.info("[Proxmox-DRS] Migration status: {0}".format(status))
    else:
        print("[Proxmox-DRS] Only 1 VM on {0} so leaving it there".format(s_node))
        my_logger.info("[Proxmox-DRS] Only 1 VM on {0} so leaving it there".format(s_node))    
    

def pv_maintenaceMode(inMaint):
    # Put a node into maintenance mode by migrating all VMs off to other nodes.
    
    print("[Proxmox-DRS] {0} is going into maintenance mode".format(inMaint))
    my_logger.info("[Proxmox-DRS] {0} is going into maintenance mode".format(inMaint))    
    print("[Proxomx_DRS] Evacuating all VMs from {}".format(inMaint))
    my_logger.info("[Proxomx_DRS] Evacuating all VMs from {}".format(inMaint))

    # Get all VMs on the node
    pv_vms = pv_getVMs(inMaint)

    # Loop through the VMs and migrate them off to other nodes
    for vm in pv_vms:
        print("[Proxomx_DRS] Evacuating {}".format(vm['name']))
        my_logger.info("[Proxomx_DRS] Evacuating {}".format(vm['name']))
        
        pv_load = pv_getLoad(inMaint)
        pv_load = sorted(pv_load, key=operator.itemgetter('index'))
        
        task_id = pv_migrate(vm['vmid'], inMaint, pv_load[0]['node'])
        
        status = pv_getTasks(task_id)
        print("[Proxmox-DRS] Migration status: {0}".format(status))
        my_logger.info("[Proxmox-DRS] Migration status: {0}".format(status))        
        

def pv_checkNodeExists(inMaint):
    # Check to make sure the node being put into maintenance
    # mode does exist.
    try:
        for node in proxmox.nodes.get():
            if node['node'] == inMaint:
                return(True)      
    except Exception as e:
        print("[Proxmox-DRS][ERROR] Get Node failed: {0}".format(e))
        my_logger.error("[Proxmox-DRS][ERROR] Get Node failed: {0} (pv_checkNodeExists)".format(e))
    
    return(False)    


def main():
    # drs_aggression is used to loop n times per run of the script
    
    inMaint = None
    
    # Check to see if a maintenance marker file exists.
    # If the files all VMs will be migrated off the node.
    # The node will be ignored for load balancing until the file is removed.
    # Note: only one node can be in maintenance at a time.
    for fname in os.listdir(maint_path):
        if fname.endswith('.maintenance'):
            inMaint = os.path.splitext(fname)[0]
            if pv_checkNodeExists(inMaint):
                print("[Proxmox-DRS] Found maintenance file {}".format(fname))
                my_logger.info("[Proxmox-DRS] Found maintenance file {}".format(fname))
                if len(pv_getVMs(inMaint)) > 0:
                    pv_maintenaceMode(inMaint)
                if len(pv_getVMs(inMaint)) == 0:
                    # Tag the file that the Evacuation has taken place
                    file = open(maint_path + fname, "w")
                    file.write("true")
                    file.close()
                break
            else:
                inMaint = None
                my_logger.error("[Proxmox-DRS][ERROR] Node being put into maintenance does not exist: {}".format(inMaint))
                print("[Proxmox-DRS][ERROR] Node being put into maintenance does not exist: {}".format(inMaint))
            
    for x in range(0, drs_aggression):
        # Get a list of nodes based on CPU
        pv_load = pv_getLoad(inMaint)
        if pv_load == None:
            break
        
        # order nodes by lowest CPU to highest
        pv_load = sorted(pv_load, key=operator.itemgetter('index'))
        
        # check if the last node (highest CPU) is outside of the load band
        if pv_load[-1]['load'] == 'HIGH':
            print("[Proxmox-DRS] {0} has high load".format(pv_load[-1]['node']))
            my_logger.info("[Proxmox-DRS] {0} has high load".format(pv_load[-1]['node']))
            pv_processMigration(pv_load)
                
        elif pv_load[0]['load'] == 'LOW':
            print("[Proxmox-DRS] {0} has low load".format(pv_load[-1]['node']))
            my_logger.info("[Proxmox-DRS] {0} has low load".format(pv_load[-1]['node']))
            pv_processMigration(pv_load)
        
        else:
            break
        
        # delay for CPU to settle on nodes post migration
        time.sleep(10)
    

if __name__ == "__main__":
    main()
